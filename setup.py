from setuptools import setup
setup(
    name='kms-api',
    version='1.1',
    description='Kms api',
    url='https://git.glebmail.xyz/PythonPrograms/kms_api',
    author='gleb',
    packages=['kmsapi'],
    author_email='gleb@glebmail.xyz',
    license='GNU GPL 3',
)