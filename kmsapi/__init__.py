import socket


class kms:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def verify(self, key):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.ip, self.port))
        sock.send(key.encode())
        result = sock.recv(2048)
        result = result.decode()
        sock.close()
        return result
